<!DOCTYPE html>
<html>
    <head>
        <base href="/">
        <title>To Do Manager</title>
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/bootstrap.min.css') }}">
    </head>
    <body>
        <my-app>Loading...</my-app>
        <script src="{{ elixir('js/vendor.js') }}"></script>
        <script src="{{ elixir('js/app.js') }}"></script>
        <script src="{{ elixir('js/jquery.min.js') }}"></script>
        <script src="{{ elixir('js/bootstrap.min.js') }}"></script>
    </body>
</html>