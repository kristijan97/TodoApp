import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard }            from './auth-guard.module';
import { AuthService }          from './auth.service'

import { ToDosComponent }       from './todos.component';
import { ToDoItemComponent }    from './todo-item.component';
import { WelcomeComponent }     from './welcome.component';
import { LoginComponent }       from './login.component';

const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'auth', component: LoginComponent },
  { path: 'todos',  component: ToDosComponent, canActivate: [AuthGuard] },
  { path: 'edit/:id', component: ToDoItemComponent, canActivate: [AuthGuard] },
  { path: 'create',     component: ToDoItemComponent, canActivate: [AuthGuard] },
  { path: 'login',     component: LoginComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}