import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService
{
    constructor(private http: Http, private router: Router) {}

    get token(): string {
        return localStorage.getItem('currentUser');
    }
    set token(token: string) {
        localStorage.setItem('currentUser', token);
    }

    getToken(): string {
        return localStorage.getItem('currentUser');
    }

    login(email, password): Observable<boolean> {
        return this.http.post('/api/authenticate', JSON.stringify({ email: email, password: password }))
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                if (token) {
                    this.token = token;
                    return true;
                } else
                    return false;
            });
    }

    logout(): void {
        this.token = null;
        localStorage.removeItem('currentUser');
        this.router.navigate(['/auth']);
    }

    loggedIn(): boolean {
        return (localStorage.getItem('currentUser') && tokenNotExpired('currentUser'));
    }
}