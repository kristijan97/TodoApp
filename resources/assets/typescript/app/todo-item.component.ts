import { MdComponentPortalAttachedToDomWithoutOriginError } from '@angular2-material/core/portal/portal-errors';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TodoService } from './todo.service'
import { Todo } from './todo'

@Component({
  selector: 'todo-item',
  template: `
    <h3>{{title}}</h3>
    <div>
      <input *ngIf="todo" type="hidden" name="id" [(ngModel)]="todo.id" />
      <input type="text" id="name" name="name" placeholder="Name" [(ngModel)]="todo.name"/><br />
      <input type="text" id="description" name="description" placeholder="Description" [(ngModel)]="todo.description"/><br />
      <label for="p">High priority?</label>
      <select id="p" name="priority" [(ngModel)]="todo.priority" required>
        <option value="0">No</option>
        <option value="1">Yes</option>
      </select><br /><br />

      <button type="button" (click)="back()">Back</button>
      <button type="button" (click)="save()">{{ btn }}</button>
    </div>
  `,
})

export class ToDoItemComponent implements OnInit
{
  title: string = '';
  btn: string = '';
  todo: Todo = new Todo(null, '', '', 0, 0);
  type: number;

  constructor(private ar: ActivatedRoute, private todoService: TodoService, private router: Router){}

  ngOnInit(): void {
    this.ar.params.forEach((params: Params) => {
     let id = +params['id'];

     if(!isNaN(id)) {
       this.todoService.getTodoById(id).subscribe((todo) => {
        this.todo = todo[0];
      });

       this.title = 'Edit';
       this.btn = 'Update';
       this.type = 1;
     } else {
       this.title = 'Create new todo';
       this.btn = 'Create';
       this.type = 2;
     }
   });
  }

  save(): void{
    if (this.type == 1)
      this.todoService.update(this.todo).subscribe(response => {
        if(response)
          alert("Succesfully updated!");
        else
          alert("Error!");
        });
    else
      this.todoService.create(this.todo).subscribe(response => {
        if(response)
          alert("Succesfully created!");
        else
          alert("Error!");
        });
  }

  back(): void {
    this.router.navigate(['/todos']);
  }
}
