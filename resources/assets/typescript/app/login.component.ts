import { WindowTimeSignature } from 'rxjs/operator/windowTime';
import { AppComponent } from './app.component';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
    template: `<form name="form" (ngSubmit)="f.form.valid && login()" #f="ngForm" novalidate>
                    <div class="form-group" [ngClass]="{ 'has-error': f.submitted && !email.valid }">
                        <label for="username">E-Mail</label>
                        <input type="text" class="form-control" name="email" style="max-width: 200px;" [(ngModel)]="model.email" #email="ngModel" required />
                        <div *ngIf="f.submitted && !email.valid" class="help-block">E-Mail is required</div>
                    </div>
                    <div class="form-group" [ngClass]="{ 'has-error': f.submitted && !password.valid }">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" style="max-width: 200px;" [(ngModel)]="model.password" #password="ngModel" required />
                        <div *ngIf="f.submitted && !password.valid" class="help-block">Password is required</div>
                    </div>
                    <div class="form-group">
                        <button [disabled]="loading" class="btn btn-primary">Login</button>
                    </div>
                    <div *ngIf="error" class="alert alert-danger">{{error}}</div>
                </form>`,
    providers: [AppComponent],
})

export class LoginComponent implements OnInit
{
    model = {
        'email': '',
        'password': ''
    };
    error = '';

    constructor(private router: Router, private authService: AuthService) {}

    ngOnInit() {
        this.authService.logout();
    }

    login() {
        this.authService.login(this.model.email, this.model.password)
            .subscribe(result => {
                if (result === true)
                    window.location.href = '/welcome';
                else 
                    this.error = 'Username or password is incorrect';
            });
    }
}