export class Todo
{
    constructor(public id: number, public name: string, public description: string, public priority: number, public completed: number) {}
}