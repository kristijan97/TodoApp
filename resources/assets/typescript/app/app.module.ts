import { NgModule, enableProdMode, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MdButtonModule } from '@angular2-material/button';
import { Router } from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { MdInputModule } from '@angular2-material/input';
import { Observable } from 'rxjs/Observable';
import { HttpModule, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.module';
import {  } from './auth-guard.module';

import { AppComponent }         from './app.component';
import { Todo }                 from './todo';
import { ToDosComponent }       from './todos.component';
import { ToDoItemComponent }    from './todo-item.component';
import { TodoService }          from './todo.service';
import { WelcomeComponent }     from './welcome.component';
import { LoginComponent }       from './login.component';

enableProdMode();

@NgModule({
  imports: [ 
    BrowserModule, 
    FormsModule, 
    MdInputModule, 
    MdButtonModule,
    AppRoutingModule,
    HttpModule
  ],

  declarations: [ 
    AppComponent,
    ToDosComponent,
    ToDoItemComponent,
    WelcomeComponent,
    LoginComponent
  ],

  bootstrap: [ AppComponent ],
  providers: [ TodoService, AuthService, AuthHttp, AUTH_PROVIDERS, AuthGuard ]
})

export class AppModule { }