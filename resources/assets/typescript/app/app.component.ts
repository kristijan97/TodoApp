import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service'

@Component({
  selector: 'my-app',
  template: require('./app.component.html'),
})

export class AppComponent
{
  loggedIn: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.loggedIn = this.authService.loggedIn();
  }

  logout(): void {
    this.loggedIn = false;
    this.authService.logout(); 
  }
}