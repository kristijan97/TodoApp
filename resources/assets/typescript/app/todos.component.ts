import { location } from '@angular/platform-browser/src/facade/browser';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Todo } from './todo';
import { TodoService } from './todo.service'

@Component({
  selector: 'my-todo',
  template: require('./todos.component.html'),
  //styleUrls: [require('./todos.component.css')]   cannot find module?! wtf
})

export class ToDosComponent
{
  todos: Todo[] = [];
  todo: Todo;

  constructor(private todoService: TodoService, private router: Router)
  {
    todoService.getAll().subscribe((todos) => {
      this.todos = todos;
    })
  }

  edit(id: number): void {
      this.router.navigate(['/edit/' + id]);
  }

  delete(todo: Todo): void {
    this.todoService.delete(todo.id).subscribe(response => {
        let index: number = this.todos.indexOf(todo);
        delete this.todos[index];

        if(response)
          alert("Succesfully completed!");
        else
          alert("Error!");
    });
  }

  complete(todo: Todo): void {
    this.todoService.completeTask(todo).subscribe(response => {
      if(response)
        alert("Succesfully completed!");
      else
        alert("Error!");
    });
  }
}
