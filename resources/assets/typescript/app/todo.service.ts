import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';

import { Todo } from './todo'
import { AuthService } from './auth.service'

@Injectable()
export class TodoService
{
    constructor(private http: Http,private authService: AuthService) {}

    getAll(): Observable<Todo[]> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);

        return this.http.get('/api/todo', {headers: headers}).map((response) => {
            return response.json() as Todo[]
        });
    }

    getTodoById(id: number): Observable<Todo> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);

        return this.http.get('/api/todo/' + id, {headers: headers}).map((response) => {
            return response.json() as Todo
        });
    }

    create(todo: Todo): Observable<boolean> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);

        return this.http.post('/api/todo', todo, { headers: headers })
                .map((response) => {
                    return response.json().success as boolean
                 });
    }

    update(todo: Todo): Observable<boolean> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);
        todo["_method"] = "PUT";

        return this.http.post('/api/todo/' + todo.id, todo, {headers: headers})
                .map((response) => {
                    return response.json().success as boolean
                 });
    }

    delete(id: number): Observable<boolean> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);

        return this.http.delete('/api/todo/' + id, {headers: headers})
                .map((response) => {
                    return response.json().success as boolean
                 });
    }

    completeTask(todo: Todo): Observable<boolean> {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authService.token);
        todo.completed = 1;
        todo["_method"] = "PUT";

        return this.http.post('/api/todo/' + todo.id, todo, {headers: headers})
                .map((response) => {
                    return response.json().success as boolean
                 });
    }
}
