<?php

use Illuminate\Database\Seeder;
use App\User;

class create_admin extends Seeder
{

    public function run()
    {
        User::create(['name' => 'admin', 'email' => 'admin@todo.com', 'password' => bcrypt('admin123')]);
    }
}
