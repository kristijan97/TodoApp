<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['name', 'description', 'priority', 'completed'];

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
