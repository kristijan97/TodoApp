<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Requests;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        $all = $request->json()->all();
        try {
            if (! $token = JWTAuth::attempt($all)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }
}
