<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Todo;
use Auth;
use Validator;

class TodoController extends Controller
{
    private function validateInput($request)
    {
        return $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'priority' => 'required|integer|between:0,1',
            'completed' => 'required|integer|between:0,1'
        ]);
    }

    function __construct() {
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        return Auth::user()->todos()->get();
    }

    public function store(Request $request)
    {
        $validator = $this->validateInput($request);

        if ($validator->fails())
            return response()->json($validator->messages(), 400);

        $newTodo = Auth::user()->todos()->create($request->all());

        return response()->json(['success' => $newTodo]);
    }

    public function show($id)
    {
        return Auth::user()->todos()->whereId($id)->get(['id', 'name', 'description', 'priority', 'completed']);
    }

    public function update(Request $request, $id)
    {
        $todo = Auth::user()->todos()->whereId($id);
        $validator = $this->validateInput($request);

        if(!$todo)
            return response()->json(['success' => 'false'], 400);

        if ($validator->fails())
            return response()->json($validator->messages(), 400);

        $todo->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'priority' => $request->input('priority'),
            'completed' => $request->input('completed'),
        ]);

        return response()->json(['success' => $todo]);
    }

    public function destroy($id)
    {
        $todo = Auth::user()->todos()->whereId($id);

        if(!$todo->delete())
            return response()->json(['success' => 'false'], 400);
        else
            return response()->json(['success' => 'true']);
    }
}
